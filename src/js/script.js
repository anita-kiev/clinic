$(document).ready(function(){

    function backToTop() {
        let button = $('.back-to-top');

        $(window).on('scroll', () => {
            if ($(this).scrollTop() >=50) {
                button.fadeIn();
            } else {
                button.fadeOut();
            }
        });

        button.on('click', (e) => {
            e.preventDefault();
            $('html').animate({scrollTop: 0}, 1000);
        })
    }

    backToTop();

});

// humburger

window.addEventListener('DOMContentLoaded', () => {
  const menu = document.querySelector('.menu'),
  menuItem = document.querySelectorAll('.menu_item'),
  hamburger = document.querySelector('.hamburger');

  hamburger.addEventListener('click', () => {
      hamburger.classList.toggle('hamburger_active');
      menu.classList.toggle('menu_active');
  });

  menuItem.forEach(item => {
      item.addEventListener('click', () => {
          hamburger.classList.toggle('hamburger_active');
          menu.classList.toggle('menu_active');
      })
  })
});


